<!--
Title format: "Recruiting for #(issue id of your research project)"
Example: "Recruiting for #123"
-->

<!---To learn about the Research Recruitment process please review the Recruiting handbook page at https://about.gitlab.com/handbook/engineering/ux/ux-research-training/recruiting-participants/>--->

## To be completed by the Requester

#### What is the link to the research issue/epic you're recruiting for?
<!---If it's an issue, create a relationship by pasting the issue URL into the /relate quick action below. --->

/relate ISSUE-URL

#### Who are your target participants?
<!---For example: SREs who use Kubernetes; a mix of GitLab and non-GitLab users. Are there underrepresented groups that would like to ensure you speak with, whom and how many?--->

#### What is your target sample size?
<!---For example: If you are conducting user interviews, how many people would you like to speak with?--->

#### What is the link to your screener or survey?
<!---Please note your screener or survey must be entered into Qualtrics before it can be distributed to users.

Need help getting started with your screener? Here's an example of a completed screener - https://gitlab.eu.qualtrics.com/jfe/preview/SV_cT80heuzGIlAdZH?Q_SurveyVersionID=current&Q_CHL=preview--->

To ensure that the UX Research Operations Coordinator can properly edit and field the Qualtrics screener, please also provide a Google Doc version of the screener with the qualifying & disqualifying criteria clearly labeled. Here is an [example](https://docs.google.com/document/d/12i2eVYQYyjRhaoHm5ufww67tMvqCOWKimV64CiOKopA/edit?usp=sharing) for guidance. If you have not provided the Google Doc version of your screener your coordinator will not be able to shortlist for you.

* Have you shared the screener in Qualtrics and the Google Doc with **both** UX Research Operations Coordinators?
     - [ ] cfaughnan@gitlab.com
     - [ ] levans@gitlab.com

***`The remaining questions relate to _in-person_ sessions only:`***

#### Has your screener gone through peer review?
To request a review, post in the #ux_research_team_lounge channel. Either a UX Research Operations Coordinator or UX Researcher will review and provide feedback.
- [ ] Yes
- [ ] No 

#### When are you hoping to meet with users?
<!---For example: Over the next 2 weeks, within the next month, etc.--->

#### How long will your individual sessions be?
<!--- Once you provide the length of your study, the UX Research Operations Coordinator will be able to assign the correct incentive amount.
--->

#### Would you like the UX Research Operations Coordinator assigned to your issue to shortlist for you? 
<!---Shortlisting means that the Coordinator assigned to your project will highlight potential individuals to you as they respond to the screener that may be a good fit for your study.--->
- [ ] Yes
- [ ] No 

#### Do you have any upcoming vacation time that may impact scheduling sessions for this study?

#### What is the link to your Calendly?
<!---If participants are not being sourced via Respondent, please ensure that you have done the following:
1. Limited your bookable hours: Calendly -> Event types -> 30 min meeting -> When can people book this event? -> Add your available times
2. Specified several different time periods that you'll be able to speak with users.

Please note that if you are booking participants through Respondent, you will be asked to re-enter your availability there, because we are required to book through the platform.
--->

#### What is the link to your personal Zoom link?
<!---If participants are being sourced via Respondent, then please put your Zoom Personal Link (an alias of your personal meeting URL). -->


* [ ] [Make a copy of the participant reimbursement spreadsheet](https://docs.google.com/spreadsheets/d/1moXi9orRAC2-nmlO-f0P0pnFsZu7PEspBrOjSl-Ooaw/copy) and rename the copy to match the recruiting request issue (Please use the following naming convention: <Recruiting Issue ID# - Recruitment Request> Example: _Recruiting Issue 1234 - Recruitment Request_).
* [ ] Follow the instructions in the copied spreadsheet.
* [ ] Move your copied sheet to the [UX Incentives Request](https://drive.google.com/drive/folders/1rfHKQbG4-X7COhIg33ycL6FlmEoIw0ZE) folder in the shared UX Research drive.
* [ ] Paste the link to the completed, copied spreadsheet here:

<!--- Please note you do not need to open a seperate Incentives issue once the Recruiting issue is open for User Interviews.
--->

## To be completed by the UX Research Operations Coordinator
* [ ] Unassign other Research Ops Coordinator & comment to let them know you'll be taking the request.
* [ ] Remove the ReOps::Triage label and replace with your own ReOps label.
* [ ] Add Study to [Research Ops Spreadsheet](https://docs.google.com/spreadsheets/d/1mFjo0hgNla_haDT5Xc7825WbZzaDtQ63BlmeWePRwm8/edit#gid=0)
* [ ] Sense check the screener or survey.
* [ ] Check that the requestor's Calendly link is set-up correctly.
* [ ] Distribute the screener or survey. Remember: With surveys, send to a sample of participants first.
* [ ] Schedule users [if required].
* [ ] Pay users
* [ ] Delete the copied spreadsheet from Google Drive (to reduce the PII footprint).
* [ ] Close study in Research Ops Spreadsheet

## Invited participants
| Participant | Scheduled | Interviewed | Thank you gift sent? |
| ------ | ------ | ------ | ------ | 
|  | |   | |   |
|  | |   | |   |
|  | |   | |   |


/label ~"UX ReOps Recruitment" ~"Research Coordination" ~"ReOps:: Triage"
/confidential
/assign @cfaughnan @laurenevans
