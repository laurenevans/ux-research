<!--Please answer the below questions to the best of your ability.-->

#### What did we learn?

<!-- Add information for this section after the research is complete. -->

| Results |
| ------ |
| `2-3 sentences to summarize the results` |
| - Link to Dovetail project |

#### What's this issue all about? (Background and context)

#### What are the overarching goals for the research?

<!-- 
Example of a research goal: Understand whether this proposed UI solution improves task success. 

- Goals need to be specific enough so you’ll know when you’ve reached an answer, practical in that you could answer them within the scope of a study, and actionable in that you could act on your findings once you’ve completed your research study.

- The goal is the central question that has to be answered by the research findings.

- Always start goals using a verb such as identify, understand, learn, gauge, etc.

- Limit the number of goals you define per project. Try to learn one or two things very well. This will help you connect your research to a design outcome and emerge with a clear understanding of one aspect of user behavior.
-->

#### What hypotheses and/or assumptions do you have?

<!-- 
A helpful way to think about hypothesis statements can be:
[Object A] will result in [X measure or difference] compared to [Object B]. 

It also helps to ask yourself what data will you be using to support/refute this at the end of the research. 

Qualitative examples: 
- The [new design] will result in less confusion and more satisfaction from our users compared to [old design]. 
- After conducting interviews, we will undesrstand what expectations our users have towards [current design].

Quantitative examples: 
- [Test] will show an [X percent] faster [measurement] improvement in our [new design] compared to the [old design].
- Results from [test] will tell us how our users organize and prioritize information in our [current design].
-->


#### What research questions are you trying to answer?

<!-- 
Examples: Are users not clear on how they complete the task? How will our solution benefit users? Is there another solution that could help users more with their pain points?

- What needs to be answered to move work forward? 

- These aren't the questions you'll directly ask users. 
-->

#### What persona, persona segment, or customer type experiences the problem most acutely?

#### What business decisions will be made based on this information?

#### What, if any, relevant prior research already exists?

<!-- Have a look in [Dovetail](https://dovetailapp.com/home/) to explore past solution validation research and look at our sales calls archive in [Chorus.ai](https://www.chorus.ai/) (both require getting access) -->

#### Who will be leading the research?

#### What timescales do you have in mind for the research?

#### Relevant links (script, prototype, notes, etc.)

<!-- Assign this issue to the Product Designer, Product Design Manager and Product Manager for the relevant group. If you're unsure of who that is, you can check here:
https://about.gitlab.com/handbook/product/categories/

If there is no one listed or you're unsure, assign the issue to the UX Research Director. Leaving an issue unassigned means it may go unnoticed by the UX Research Team.
-->

<!-- #### TODO Checklist
 Consider adding a checklist in order to keep track of what stage the research is up to. Some possible checklist templates are here:
 https://about.gitlab.com/handbook/engineering/ux/ux-research-training/ux-research-resources/#checklists
 -->

/label ~"workflow::solution validation"
/label ~"UX solution validation"
/label ~"UX research"
/label ~"UX Research Backlog"
/confidential
