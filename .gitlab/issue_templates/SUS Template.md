<!-- 
This template is intended to be used for the quarterly execution of SUS Research.

It is best practice to start this process at least 1 month before the end of the current quarter. This will give you enough time to setup, collect data, and report out. SUS is executed quarterly for SaaS users and every other quarter for self-managed users. For more details look at the SUS handbook page found here: https://about.gitlab.com/handbook/engineering/ux/performance-indicators/system-usability-scale/

Title the issue in the following format: System Usability Scale (SUS) Survey - Q?-FY202? - Research Execution
 -->

## SaaS (run every quarter)
#### Recruitment SaaS
1. [ ] Duplicate the SUS survey template in the shared UX research Qualtrics account  
1. [ ] Complete the [Promotional Games workflow](https://about.gitlab.com/handbook/legal/ux-research-pilot/) to generate the abbreviated rules for the drawing.
   1. [ ] Publish the survey and update the Abbreviated Rules [link](https://www.qualtrics.com/support/survey-platform/survey-module/editing-questions/rich-content-editor/insert-a-downloadable-file/) in Q12 with the newly generated rules.
1. [ ] Find SaaS users from data warehouse 
   1. [ ] Request access to Sisense as an editor to be able to query, if you don't already have access.
   1. [ ] Use [these queries](https://app.periscopedata.com/app/gitlab/881970/Nick's-SUS-Scratch) to generate a list of user IDs. 
      - SUS - Paid Segment - All Users - With Email
      - SUS - Free Segment - All Users
   1. [ ] Import mailing list into Qualtrics
      - [ ] Refresh Sisense charts that contain Free Segment - All Users with emails and Paid Segment - All Users with emails.
      - [ ] Download data, save it to your local file system, and rename it with the appropriate wave number
      - [ ] In Qualtrics, create a new email list with the appropriate name (ex. Q2FY23 SUS Free Wave 1) and upload the correct list from your local machine.
   <details><summary>Old Instructions</summary>
   - [ ] Import mailing list into Qualtrics via Google Sheets following [handbook guidance](https://about.gitlab.com/handbook/engineering/ux/qualtrics/#distributing-your-survey-to-gitlabcom-users)
      - [ ] Generate your list of GitLab.com user IDs and put them into the first column of a Google Sheet. The first entry in the first column (A1) should be id, as this confirms that this is a list of ids that you want to use.
      - [ ] The name of the specific sheet (or tab) should be what you want to name the Qualtrics mailing list, for example, `05/20 - CI Pipeline Prototype Testing`. **This name cannot contain the following characters: `. < >`. Doing so will case an error in the transfer to Qualtrics.**
      - [ ] The filename of the entire spreadsheet should be `qualtrics_mailing_list`. followed by the name of the worksheet/mailing list that you used in the step above. So keeping with our previous example, `qualtrics_mailing_list.05/20 - CI Pipeline Prototype Testing`
      - [ ] Once everything is ready, move the file into the `QualtricsRequest` Google Drive folder.
         - If you do not see the `QualtricsRequest` Google Drive folder, ask another researcher to share access.
      - [ ] If everything was done correctly, within about 15 minutes, the `id` entry in A1 should turn into `processing`. That means the data pipeline is working on your request.
         - This can take up to 1 hour for lists of 6000.
      - [ ] A1 will show `processed`. Your mailing list should now be in Qualtrics at this point.
      - [ ] Add `processed_` to the filename in order to make it clear which files have been completed, as well as to keep the process executing as quickly as possible in the future.
      - [ ] The list will show up as a shared list for the _UX Research & Product_ directory. You can access the list itself from the Contacts section of Qualtrics, or you can select the list as part of the process of sending an email distribution.
      - The GitLab.com user ID you specified in your Google Sheet will be included in the Qualtrics mailing list as embedded data. You can use this to associate any responses you get with your original user list.
      - https://about.gitlab.com/handbook/engineering/ux/ux-research-training/finding-saas-users/ 
      - https://about.gitlab.com/handbook/business-technology/data-team/#-data-warehouse
   </details>
1. [ ] Distribute survey to a test sample of users. 
   1. [ ] Within the Distributions tab in Qualtrics select Email and then click "Compose Email"
   1. [ ] In the "To" dropdown, hover over `Organization Library: UX Research & Product` and search for the name of the list you generated in Sisense.
   1. [ ] In the "Message dropdown, hover over `Organization Library: UX Research & Product` and search and select "SUS Template". Make sure information in email is updated.
   - **Note: Filling the cohorts could take several weeks so it’s good to start recruiting at the beginning of the last month in the quarter.**
   - If the target _N_ = 800 (200 Paid/Mature, 200 Paid/New, 200 Free/Mature, 200 Free/New) and response rate is 2-3%, you will need to sample between 26,666 and 40,000. Start with waves of 6000 every couple days from Free and Paid users and adjust subsequent waves according to response rate.
1. [ ] Review answers and make any necessary amendments to the survey. Distribute survey to remaining users.
1. [ ] Close survey and cleanse data 
1. [ ] Conduct prize drawing and pay participants (or request Research Coordinator’s assistance)

#### Analysis
1. [ ] Export raw numeric data from Qualtrics 
1. [ ] Transfer raw data into the corresponding sheets in the [‘Results scoring template’](https://docs.google.com/spreadsheets/d/14zNcdjR3R5OLeOFxPBjBCHhi5zIKdXh8WnEo4iGIPJY/edit?usp=sharing) sheet.
   1. [ ] Move previous quarter’s data into the sheets for previous quarters. 
      - **Note: This sheet will be shared with the entire company so contact information should be removed.** 
1. [ ] Analyze open-text responses to the following question: “Is there anything else you’d like to share with us about GitLab’s usability?” 
   - Suggested approach: copy responses into ‘Usability Verbatims’ tab of the results sheet fill in as many columns as possible. Data validation can be updated to cover new themes.
1. [ ] Create an issue for categorization by stage for the PMs and PDMs, use [this issue as a template](https://gitlab.com/gitlab-org/ux-research/-/issues/2040).
1. [ ] Update the ‘SaaS SUS Survey Scores’ dashboard in Sisense (Wait until release plan is determined)
   1. [ ] Export survey data from Qualtrics (text and numeric)
   1. [ ] Import CSVs into Sisense following the guidance in https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/10068#note_517593830 
1. [ ] Download Verbatim data (after Stage categorization) and intermediate_current as .csv's. Add those .csv's to the `SUS Sisense Upload` folder of the `System Usability Scale` folder in the UX Research google drive.

## Self-managed (run every other quarter - Q1 & Q3)
#### Recruitment Self-managed
1. [ ] Duplicate the SM SUS survey template in the shared UX research Qualtrics account 
1. [ ] Complete the [Promotional Games workflow](https://about.gitlab.com/handbook/legal/ux-research-pilot/) to generate the abbreviated rules for the drawing, if you haven't already done this for the SaaS iteration.
   1. [ ] Publish the survey and update the Abbreviated Rules [link](https://www.qualtrics.com/support/survey-platform/survey-module/editing-questions/rich-content-editor/insert-a-downloadable-file/) in Q12 with the newly generated rules. (Note: you can use the same abbreviated rules for the SM and SaaS surveys, but you have to make sure the updated rules are in both surveys).
   1. [ ] Share Qualtrics survey with UX Research Coordinators.
1. [ ] Reach out to a UX Research Coordinator to help generate Marketo waves for distribution.
   - This process should be started about a month and a half before the end of the quarter. Collecting SM data via Marketo can take a while.
1. [ ] Close Survey and clean data
1. [ ] Make sure prize drawing includes names from both the SaaS and SM lists.

#### Reporting & Sharing
1. [ ] Create a slide deck to share key findings ([Sample quarterly slide deck](https://docs.google.com/presentation/d/1crS0oQ91Tr895FEk0Qmm3KTe9F_T5y9CDuSIBtFA87M/edit?usp=sharing)) 
1. [ ] Document survey results as an epic and add the epic to the following parent epic: https://gitlab.com/groups/gitlab-org/-/epics/1455 
1. [ ] Create issue to accurately categorize SUS verbatim by stages ([Sample Issue](https://gitlab.com/gitlab-org/ux-research/-/issues/1645))
1. [ ] Create a separate sheet that contains participants who are interested in a follow-up conversation ([sample follow-up sheet](https://docs.google.com/spreadsheets/d/1OkfdvF3iyzndeCRAWLCJkoLRTB3CCl8NYgVEkTSi9eU/edit?usp=sharing); [handbook overview of process](https://about.gitlab.com/handbook/engineering/ux/performance-indicators/system-usability-scale/sus-outreach.html))
1. [ ] Add verbatims to the [SUS Themes by Usability Issues sheet](https://docs.google.com/spreadsheets/d/1rJMJIt759FqAtKgJY-WuOPPRNwvSvOAgy4fkcOxI0ic/edit?usp=sharing)

#### Associated Resources
- [Results scoring template](https://docs.google.com/spreadsheets/d/14zNcdjR3R5OLeOFxPBjBCHhi5zIKdXh8WnEo4iGIPJY/edit?usp=sharing)
- [Epic with documented survey results](https://gitlab.com/groups/gitlab-org/-/epics/1455)
- [SUS usability issues by theme sheet](https://docs.google.com/spreadsheets/d/1rJMJIt759FqAtKgJY-WuOPPRNwvSvOAgy4fkcOxI0ic/edit?usp=sharing)
- [SUS verbatims across quarters sheet](https://docs.google.com/spreadsheets/d/1nLRH2rstmz_frvumx30eFA-v4XPuEYbpIaT_rMLDwnc/edit?usp=sharing)
- [Sisense dashboard for SaaS scores](https://app.periscopedata.com/app/gitlab/816864/SaaS-SUS-Survey-Scores)
- [Sisense dashboard for self-managed scores](https://app.periscopedata.com/app/gitlab/863612/Self-Managed-SUS-Survey-Scores)
- [Overview of SUS themes slide deck](https://docs.google.com/presentation/d/1kMZcRnOjuQ0K3TtT1YxedhFb5-Ej_cr2mXxeeJ9sbZ0/edit#slide=id.g469c9b8c47_0_0)
- [Sample quarterly slide deck](https://docs.google.com/presentation/d/1crS0oQ91Tr895FEk0Qmm3KTe9F_T5y9CDuSIBtFA87M/edit?usp=sharing) 
- [System Usability Scale video series](https://www.youtube.com/playlist?list=PL05JrBw4t0KrPNM6WFlrsVM8tWwkYDyio)

#### Prior Survey Data
- [Raw data for Sisense dashboard](https://docs.google.com/spreadsheets/d/14lGcCyTMqUCXHhpDb7O3s3kc4wfQIDhzlb15WKY5BDk/edit#gid=892814487)
- [Prior quarter results sheets (google drive)](https://drive.google.com/drive/folders/1Me5_0lcyaFJ6egbB6aytS7di1cGyV0wz?usp=sharing)

/label ~"SUS Survey"

/label ~"UX Research"
