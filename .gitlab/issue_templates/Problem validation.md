<!-- This template is used for executing a Problem Validation study. Learn more about it in the handbook: 
- https://about.gitlab.com/handbook/engineering/ux/ux-research-training/problem-validation-and-methods
- https://about.gitlab.com/handbook/product-development-flow/#validation-phase-2-problem-validation

Assign this issue to the researcher for your group. If you're unsure which researcher is assigned to your group, you can check:
https://about.gitlab.com/handbook/product/categories/

If there is no one listed or you're unsure, assign the issue to the UX Research Director (@asmolinski2). Leaving an issue unassigned means it may go unnoticed by the UX Research Team.
-->

#### What did we learn?

<!-- Add information for this section after the research is complete. -->

| Results |
| ------ |
| `2-3 sentences to summarize the results` |
| `Link to Dovetail project` |

---

<!-- Please answer the below questions to the best of your ability.-->

#### What's this issue all about? (Background and context)

#### What are the overarching goals for the research?

<!-- 
Example of a research goal: Understand how people use GitLab to deploy applications.

- Goals need to be specific enough so you’ll know when you’ve reached an answer, practical in that you could answer them within the scope of a study, and actionable in that you could act on your findings once you’ve completed your research study.

- The goal is the central question that has to be answered by the research findings.

- Always start goals using a verb such as identify, understand, learn, gauge, etc.

- Limit the number of goals you define per project. Try to learn one or two things very well. This will help you connect your research to a design outcome and emerge with a clear understanding of one aspect of user behavior.
-->

#### What hypotheses and/or assumptions do you have?

####  What research questions are you trying to answer?

<!-- 
Examples: Are people not paying for a subscription because they don’t like the features we have? Do we have issues with the website? Are our prices too high? Are we missing features that customers are looking for?

- What needs to be answered to move work forward? 

- These aren't the questions you'll directly ask users. 
-->

#### What persona, persona segment, or customer type experiences the problem most acutely?

#### What business decisions will be made based on this information?

#### What, if any, relevant prior research already exists?

<!-- Have a look in [Dovetail](https://dovetailapp.com/home/) to explore past problem validation research and look at our sales calls archive in [Chorus.ai](https://www.chorus.ai/) (both require getting access) -->

#### What timescales do you have in mind for the research?

#### Who will be leading the research?

#### Relevant links (opportunity canvas, discussion guide, notes, etc.)

<!-- #### TODO Checklist
 Consider adding a checklist in order to keep track of what stage the research is up to. Some possible checklist templates are here:
 https://about.gitlab.com/handbook/engineering/ux/ux-research-training/ux-research-resources/#checklists
 -->

/label ~devops:: ~category: ~group::
/label ~"workflow::problem validation"
/label ~"UX problem validation"
/label ~"UX research"
/label ~"UX Research Backlog"
/confidential 
