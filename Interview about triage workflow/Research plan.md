# Interview about triage workflow – Research plan

## Research questions
1. How do users triage incidents?
     1. What are the steps that users need to take?
     1. What tools do they use? Do they need to switch in between tools often?
     1. Do they need to transfer data or information in between tools? How is this done?
     1. What problems do users encounter when triaging incidents?

## Method
### Data collection
We will conduct semi-structured interviews where participants will describe 
in detail how they triaged incident recently and further discuss how 
did this triage workflow differed for triaging other incidents that they 
recall. Except for the workflow, the focus of this interview will be tools that participant 
use while performing the triage and problems that they face.

### Data analysis
For each participant, we will document their triage workflow. We will compile a list of 
all tools and problems mentioned during the sessions. We will identify
and document common themes and patterns in the workflows.

### Recruitment
For 1 hour long-sessions we will recruit 5 participants from GitLab FirstLook 
panel (or via Respondent) that meet the following requirements: 

 - Identifies with relevant JTBD: Triage incidents, Perform root cause analysis
 - Identifies with relevant role: DevOps Engineer

Specific questions and criteria can be found in [Screener](/Screener.md)

## Procedure
1. General intro
1. Initial interview
1. Topic intro
1. Incident description
1. Workflow of triaging the incident
1. Similarities and differences with other incidents

Procedure is described in detail in the [Session guide](/Session guide.md) 
document. 
