# Feature description guideline

## Guidelines
- Clear and sufficiently detailed description that sets the feature into 
necessary system context.
- Simple language without GitLab lingo or sales pitching
- Describe proper features, not MVCs. 

### Feature description structure
- Feature: What can the user see in the UI
- Behavior: What does it do
- Value: Why do we want to introduce it
